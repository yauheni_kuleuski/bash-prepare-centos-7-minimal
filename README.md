Develop BASH script that should do the following:

1. configure network to have ssh access from local machine;
2. restrict ssh connections for root user and to single ssh port(iptables);
3. add local DVD ISO as an repo(all internet repos need to be removed);
4. set up RAID5 on 3 more drives(the size is not matter);
5. set up LVM for the RAID;
6. create xfs on top of it;
7. mount it to any folder;
8. explode the folder into the network with NFS
The script should:

- has logging on stdout;
- has error handling;
- should receive input parameters: block devices that will be used and mount point.
VM should:

- has host only one NIC interface;
- CentOS 7.x Minimal installed with default setting;
- should has 3 additional drives connected. size is not matter here. let's say 100Mb each"
