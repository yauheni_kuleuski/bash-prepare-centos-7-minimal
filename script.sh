#!/usr/bin/bash

# If you have next error
# bash: script.sh: /usr/bin/bash^M: bad interpreter: No such file or directory

# please, check this instruction:
# https://stafox.ru/error-bash-configure-bin-sh-m-bad-interpreter-no-such-file-or-directory/

# ===============================================================================================
# 								Default settings
# ===============================================================================================

# Variables:
SSH_CONFIG_PATH="/etc/ssh/sshd_config"
ISO_PATH=$(blkid | grep -i iso | grep -i "centos 7" | cut -d : -f 1)
LOCAL_REPO_PATH="/mnt/CentOS7DVD"
SD_COUNT=$(ls /dev/* | grep '/dev/sd[^a]$' | wc -l)
DEFAULT_BLOCK_DEVICES=$(echo $(ls /dev/sd[!a]) | cut -d " " -f 1-3)
DEFAULT_RAID=$(ls /dev/md*)
USED_RAID="/dev/md0"
DEFAULT_LVM_PATH="/mnt/lv_dirs"
VOLUME_GROUP="vg1"
LOGICAL_VOLUME="lv1"
REPOS_FILES=$(ls /etc/yum.repos.d/*)
USERS_BACKUP="/root/backup"

# ===============================================================================================
# 								Functions
# ===============================================================================================

assignment_basic_devices () {
	echo -e "\nHello $USER\nBefore we begin, I need some information from you."
	echo -e "\nYou can select any other of the connected drives presented below:\n$(echo $(ls /dev/sd[!a]))"

	while read -p "Enter three absolute paths separated by spaces: " -a USERS_DEVICES; do
		if [ ${#USERS_DEVICES[@]} -eq 3 -a -b ${USERS_DEVICES[0]} -a -b ${USERS_DEVICES[1]} -a -b ${USERS_DEVICES[2]} ]; then
			SD1=${USERS_DEVICES[0]} && SD2=${USERS_DEVICES[1]} && SD3=${USERS_DEVICES[2]} &&
			echo -e "\nDevices for RAID5 array detected."
			echo -e "\t$SD1\t$SD2\t$SD3\n"
			sleep 1
			break
		else
			echo -e "\nYou have entered insufficient variables or they do not exist. Please enter valid data.\n"
		fi
	done
}

assignment_mount_point () {
	while read -p "Select mount point that will be used? (mount point for LMV folder): " -a MOUNT_POINT; do
		if [ ${#MOUNT_POINT[@]} -eq 1 ]; then
			echo -e "\nMount points detected."
			echo -e "\t${MOUNT_POINT[0]}"
			sleep 2
			break
		else
			echo -e "\nPlease enter mount point.\n"
		fi
	done
}

install_packages () {
	yum install -y mdadm
	yum install -y nfs-utils nfs-utils-lib nfs*
}

disks_preparation () {
	wipefs --all --force $SD1 $SD2 $SD3
	umount "$(ls /dev/mapper/${VOLUME_GROUP}-${LOGICAL_VOLUME})"
	if [ "$(cat /etc/fstab | grep "${MOUNT_POINT[0]}")" ]; then
		sed -i '$d' /etc/fstab
	fi
	lvremove -y /dev/vg?/lv?
	vgremove -y /dev/vg?
	pvremove -y $USED_RAID

	if [ "$USED_RAID" ]; 
	then
		mdadm -S $USED_RAID
	else
		echo "RAID arrays not detected."
	fi
	mdadm --zero-superblock --force $SD1 $SD2 $SD3
	echo -e "\Disks preparation: COMPLETE"
}

ssh_configuration () {
	if [ -n "$(cat $SSH_CONFIG_PATH | grep -i 'PermitRootLogin yes')" ];
	then
		sed -i 's/#PermitRootLogin yes/PermitRootLogin no/' $SSH_CONFIG_PATH
	else
		echo -e "\n# Restrict SSH connections for ROOT user\nPermitRootLogin no" >> $SSH_CONFIG_PATH
	fi
	systemctl restart sshd
	echo -e "\nRestrict ssh connections for root user: DONE"
}

iptables_configuration () {
	[ "$(systemctl disable firewalld | grep "active")" ] && systemctl disable firewalld
	yum install -y iptables-services 2>&1
	systemctl enable iptables
	iptables -F
	iptables -A INPUT -p tcp --dport 22 -j ACCEPT
	iptables -A OUTPUT -p tcp --sport 22 -j ACCEPT
	echo -e "\nAll ports are closed except SSH(iptables): DONE"
}

iso_mounting () {
	if [ "$ISO_PATH" ];
	then
		if [ "$(mount -l | grep "$ISO_PATH")" ]; then
			echo -e "CentOS 7 ISO already mounted." >&2
		else
			mkdir -p $LOCAL_REPO_PATH &&
			mount $ISO_PATH $LOCAL_REPO_PATH

			mkdir -p $USERS_BACKUP/repos &&
			mv $REPOS_FILES $USERS_BACKUP/repos

			echo -e "[LocalRepo]\nname=LocalRepository\nbaseurl=file://$LOCAL_REPO_PATH\nenabled=1\ngpgcheck=1\ngpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7" > /etc/yum.repos.d/local.repo
			yum clean all
			yum update
			echo -e "\n	Add local DVD ISO as an repo(all internet repos need to be removed): DONE"
		fi
	else
		echo "ISO image not found." >&2
	fi
}

raid_array_initialization () {
	mdadm --create --verbose $USED_RAID -l 5 -n 3 $SD1 $SD2 $SD3
	[ -d "/etc/mdadm" ] || mkdir /etc/mdadm
	echo "DEVICE partitions" > /etc/mdadm/mdadm.conf
	mdadm --detail --scan --verbose | awk '/ARRAY/ {print}' >> /etc/mdadm/mdadm.conf
}

lvm_configuration () {
	pvcreate $USED_RAID
	vgcreate $VOLUME_GROUP $USED_RAID
	vgmknodes $VOLUME_GROUP
	VG_SIZE=$(($(($(fdisk -l $USED_RAID | grep $USED_RAID | cut -d " " -f 3) * 90)) / 100 ))
	lvcreate -y -L ${VG_SIZE}M -n $LOGICAL_VOLUME $VOLUME_GROUP
	echo -e "\nThe LVM for the RAID5 array is CONFIGURED."
}

file_system_creating () {
	mkfs.xfs /dev/$VOLUME_GROUP/$LOGICAL_VOLUME
	echo -e "\nXFS on logical volume CREATED."
}

lvm_mounting () {
	[ -d ${MOUNT_POINT[0]} ] || mkdir -p ${MOUNT_POINT[0]} && chmod -R 777 ${MOUNT_POINT[0]}
	[ "$(mount -l | grep /dev/mapper/${VOLUME_GROUP}-${LOGICAL_VOLUME})" ] && echo "${LOGICAL_VOLUME} already mounted." ||
	mount /dev/mapper/${VOLUME_GROUP}-${LOGICAL_VOLUME} ${MOUNT_POINT[0]}
	if [ -z $(cat /etc/fstab | grep "/dev/$VOLUME_GROUP/$LOGICAL_VOLUME") ]; then
		echo -e "/dev/mapper/${VOLUME_GROUP}-${LOGICAL_VOLUME} ${MOUNT_POINT[0]}\t\t\txfs\tdefaults\t\t0 0" >> /etc/fstab
	fi
	echo -e "\nLogical volume with XFS file system mounted on ${MOUNT_POINT[0]}"
}

nfs_shared_folder () {
	service rpcbind start
	service nfs start
	/etc/init.d/network start
	echo "${MOUNT_POINT[0]} 192.168.0.0/24(rw,sync,no_root_squash)" >> /etc/exports
	echo -e "\nportmap: ALL\nlockd: ALL\nMountd: ALL\nrquotad: ALL\nstatd: ALL" >> /etc/hosts.deny
	echo -e "\nportmap: 192.168.0.208\nlockd: 192.168.0.208\nMountd: 192.168.0.208\nrquotad: 192.168.0.208\nstatd: 192.168.0.208" >> /etc/hosts.allow
	service rpcbind restart
	service nfs restart
	/etc/init.d/network restart
	port=$(rpcinfo -p | grep nfs | head -1 | awk '{print $4}')
	echo -e "localhost:/root  ${MOUNT_POINT[0]}  xfs  rw,hard,intr,$port  0 0"
	iptables -A INPUT -p tcp --dport $port -j ACCEPT
	iptables -A OUTPUT -p tcp --sport $port -j ACCEPT
	iptables -A INPUT -j DROP
	iptables -A OUTPUT -j DROP
	service iptables save
	echo -e "\nExplode the folder into the network with NFS"
}

system_information () {
	echo -e "\nSSH information:"
	systemctl status sshd | head -n 3
	echo -e "\nIPTABLES information:"
	iptables -L INPUT && echo && iptables -L OUTPUT
	echo -e "\nRAID5 information:"
	mdadm -D $USED_RAID | head -n 10
	mdadm -D $USED_RAID | tail -n 4
	echo -e "\nLVM information:"
	lvdisplay "/dev/$VOLUME_GROUP/$LOGICAL_VOLUME"
	echo -e "\nMOUNT information:"
	mount -l | tail -n 4
}

menu () {
	clear
	echo -e "\n\t\t\tScript menu:\n"
	echo -e "\t1. Make customization"
	echo -e "\t2. Reassign devices"
	echo -e "\t3. Clear selected drives to configure"
	echo -e "\t4. Print full information"
	echo -e "\t0. Exit"
	echo -en "\t\tEnter section number:\n\n"
	read -n 1 option
}

# ===============================================================================================
# 								Script body
# ===============================================================================================

clear
assignment_basic_devices
assignment_mount_point

while [ $? -ne 1 ]; do
	menu
	case $option in
		0)		break ;;
		1)		echo -e "\n\nStart system customization"
				# Functions
					disks_preparation 1>> report.txt 2>> errors.txt
					iso_mounting 1>> report.txt 2>> errors.txt
					install_packages 1>> report.txt 2>> errors.txt
					ssh_configuration 1>> report.txt 2>> errors.txt
					iptables_configuration 1>> report.txt 2>> errors.txt
					raid_array_initialization 1>> report.txt 2>> errors.txt
					lvm_configuration 1>> report.txt 2>> errors.txt
					file_system_creating 1>> report.txt 2>> errors.txt
					lvm_mounting 1>> report.txt 2>> errors.txt
					nfs_shared_folder 1>> report.txt 2>> errors.txt
				echo -e "\nSystem customization finished."
				echo -e "\nFor full information please look at section 4." ;;

		2)		assignment_basic_devices
				assignment_mount_point ;;

		3)		disks_preparation 1>> report.txt 2>> errors.txt ;;

		4)		system_information ;;
		*)		clear
				echo "Need to select a section";;
	esac
	echo -en "\n\n\t\t\tPress any key to continue"
	read -n 1 line
done
clear
